//[SECTION] Iterator Methods

   // What are Iteration Methods?
   // --> Iteration methods are loops designed to perform repetitive tasks/procedures on array data structures.
   // --> There are very useful for manipulating array data sets resulting in complex tasks.

// forEach()

     //Description:
     	//=> similar to a 'for loop' method that will allow to access and iterate each element inside an array. 

     //SYNTAX:
        // arrayName.forEach( function(element) {
        // 	statement/procedure
        // })

     //Example:
     let tasks = [
     	'eat HTML',
     	'drink JS',
     	'throw Bootstrap',
     	'sleep with React'
     ];

     //NOTE: normally, as a writing convention, when selecting a variable to describe an array or collection it should be in 'PLURAL' form. 

     //it is also a common practive to use the 'singular form' of the array name when describing the content of the collection
     tasks.forEach( function(task) {
     	//we can now describe what we want to happen to each element inside the array collection.
     	//display all elements one by one in the terminal. 
     	console.log(`THIS IS MY TASK ${task}`); 
     	//it will upon the use case on how you want to return the elements that will be iterated from the array collection. 
     });

     //Example: use forEach with a conditional statement. 

     const jobs = ['eat', 'pray', 'sleep', 'wine', 'dine'];

     //Lets filter the collection and only accept jobs that are more than 3 letters. 

     const acceptedJobs = []; 

     jobs.forEach( function(job) {
        //state the procedure of what you want to happen in order to accomplish the task. 
        if (job.length === 3) {
        	//accept the task
        	//what mutator can you use to add an element inside an array?
        	acceptedJobs.push(job);
        }
     });

     console.log(jobs); 
     console.log('Here are the list of Accepted Jobs:');
     console.log(acceptedJobs); 

     //map() -> this function access and iterates each element inside an array and returns a NEW ARRAY with different values depending on the function's procedure. 

     //SYNTAX: 
         // let/const resultArray = arrayName.map( function(arrayElement) {
         // 	statement
         // })

     //EXAMPLE: 
       let numbers = [6, 12, 13, 14, 15]; 

       //using the map method lets iterate each element inside the array and multiply it by itself and store inside a new variable.

       //for better use case of the map() -> when you to execute a certain procedure on the elements of an array and directly store its outcome into a new array. 
       let mapOutcome = numbers.map( function(number) {
 		     	return number * number; //the sum of the operation will become the designated return 	
       }); 


       console.log('Result of the Map method'); 
       console.log(mapOutcome); 
       console.log(numbers); 

    //every() -> this function will allow us to evaluate all the elements inside an array structure, if the data sets meet a given condition. the return of the every() will be a boolean(TRUE/FALSE) data type.

    //Beneficial/Useful -> this is useful whenever you will validate data stored in an array especially when dealing with large amounts of data. 

    //SYNTAX: 

          // arrayName.every( function(element) {
          //    //statement / condition
          // })

          //given the available data sets in the numbers array check if all of the numbers are greater than the value of 10.

          let allGreaterThan10 = numbers.every( function(number) {
              return number > 10;
          }); 

          //lets diplay the return in the console
          console.log(allGreaterThan10); //true

     //Example#2: 
      
      //water, coffee, martini, beer
      let beverages = ['🥛', '🥛', '🥛', '🥛'];  

      //using the every(). create a logic that will check if ALL beverages are free from alchohol or caffein. 

      let allSafeDrinks = beverages.every( function(beverage) {
         return beverage === '🥛' //true/false
      });  

      console.log(allSafeDrinks); 
   
   //some() -> is to evaluate and check if atleast one element would pass a given condition, and RETURNS  a value of 'true' if one element meets the condition and 'false' if otherwise. 

   //SYNTAX:
      // let/const name = arrayName.some( function() {
      //    //logical statement or condition
      // }); 

    //you are the manager of a fast foodchain, and now taking orders online, create a logic that will make sure that will make sure if you will be able to provide atleast one of the orders from your customer. 
    let orders = ['🍕','🍥','🍔','🥡','🥩'];

    //unfortunately from the orders above you can only serve burgers and fries. 

    let canDeliverSomeOrders = orders.some( function(order) {
        return order === '🍔' || order === '🍟';
    }); 

    console.log(canDeliverSomeOrders); //true or false?

    //example#2: 

    let grades = [98, 97, 90, 98, 96]; 

    //imagine that you are a teacher who is trying to identify if a student is qualified for high honors or highest honors.

    //=> highest honor => no grade below 90. 

    let isHighHonor = grades.some( function(grade) {
        return grade <= 89; 
    }); 

    console.log(isHighHonor); //true || false?


